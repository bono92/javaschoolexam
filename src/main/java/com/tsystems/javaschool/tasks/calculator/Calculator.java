package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {

    private static final List<String> OPERATIONS = Arrays.asList("+", "-", "*", "/", "(", ")", "|");

    private List<String> rpnArray = new ArrayList<>();
    private Deque<String> tempArray = new LinkedList<>();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            Objects.requireNonNull(statement);
            checkValidExpressionByBrackets(statement);
        } catch (Exception e) {
            return null;
        }

        statement = (statement + "|").replace(" ", "");
        for (String myOperator : OPERATIONS) {
            statement = statement.replace(myOperator, " " + myOperator + " ");
        }
        String units[] = statement.split(" ");


        for (String unit : units) {

            if (unit.equals("")) {
                continue;
            }

            if (fillArrays(unit) == 0) {
                return null;
            }

        }


        Stack<Double> stackDouble = new Stack<Double>();

        for (String elem : rpnArray) {

            if (isOperand(elem)) {

                stackDouble.add(Double.parseDouble(elem));

            } else {

                try {
                    double y = stackDouble.pop();
                    double x = stackDouble.pop();

                    switch (elem) {
                        case "+":
                            stackDouble.add(x + y);
                            break;
                        case "-":
                            stackDouble.add(x - y);
                            break;
                        case "*":
                            stackDouble.add(x * y);
                            break;
                        case "/":
                            stackDouble.add(x / y);
                            break;
                    }
                } catch (EmptyStackException e) {
                    return null;
                }


            }

        }


        try {
            Double result = stackDouble.pop();
            BigDecimal roundedResult = new BigDecimal(result);
            roundedResult = roundedResult.setScale(4 ,BigDecimal.ROUND_HALF_DOWN);
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("###.####", decimalFormatSymbols);
            return df.format(roundedResult);
        } catch (Exception e) {
            return null;
        }

    }

    private void checkValidExpressionByBrackets(String statement) {
        char[] chars = statement.toCharArray();
        int balanceBrackets = 0;
        for (char c : chars) {
            if (c == '(') balanceBrackets++;
            if (c == ')') balanceBrackets--;
        }
        if (balanceBrackets != 0)
            throw new IllegalArgumentException();
    }

    private int fillArrays(String unit) {

        if (isOperator(unit)) {
            int operation = getOperation(unit);

            if (operation == 1) {
                tempArray.add(unit);
            } else if (operation == 2) {
                rpnArray.add(tempArray.removeLast());
                fillArrays(unit);

            } else if (operation == 3) {
                tempArray.removeLast();
            } else if (operation == 5) {
                return 0;
            }
        } else if (isOperand(unit)) {
            rpnArray.add(unit);
        } else {
            return 0;
        }
        return 1;
    }

    private int getOperation(String currentUnit) {

        String lastOperator = tempArray.isEmpty() ? "|" : tempArray.peekLast();

        switch (lastOperator) {
            case "|":
                switch (currentUnit) {
                    case "|":
                        return 4;
                    case ")":
                        return 5;
                    default:
                        return 1;
                }
            case "+":
            case "-":
                if (Objects.equals(currentUnit, "*") || Objects.equals(lastOperator, "/") || Objects.equals(lastOperator, "(")) {
                    return 1;
                } else {
                    return 2;
                }
            case "*":
            case "/":
                return Objects.equals(currentUnit,"(") ? 1 : 2;
            case "(":
                switch (currentUnit) {
                    case "|":
                        return 5;
                    case ")":
                        return 3;
                    default:
                        return 1;
                }
        }
        return 0;
    }

    private boolean isOperand(String current) {
        try {
            Double.parseDouble(current);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOperator(String unit) {
        return OPERATIONS.stream().anyMatch(unit::equals);
    }


}
